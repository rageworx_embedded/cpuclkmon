// Programmed by Raphael Kim.
// 

// SYSTEM -
#include <unistd.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <termios.h>

// STD-C
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>

// STD-C++
#include <string>
#include <vector>

#include <pthread.h>

#include "version.h"

using namespace std;

/////////////////////////////////////////////////////////////////////////

#define DEF_APP_NAME        "RaphKay's Embedded CPU clock monitor"

#define DEF_VIRTUAL_TEMP_MAP "/sys/devices/virtual/thermal/thermal_zone%u/temp"
#define DEF_CPU_PRESENT     "/sys/devices/system/cpu/present"
#define DEF_CPU_MAX_CLK_MAP "/sys/devices/system/cpu/cpu%u/cpufreq/scaling_max_freq"
#define DEF_CPU_MIN_CLK_MAP "/sys/devices/system/cpu/cpu%u/cpufreq/scaling_min_freq"
#define DEF_CPU_CUR_CLK_MAP "/sys/devices/system/cpu/cpu%u/cpufreq/scaling_cur_freq"

/////////////////////////////////////////////////////////////////////////

static unsigned         cpu_cnt_min = 0;
static unsigned         cpu_cnt_max = 0;
static vector< float >  cpu_freq_min;
static vector< float >  cpu_freq_max;
static vector< float >  cpu_freq_cur;
static float            cpu_freq_maxall = 0.f;
static float            cpu_temper = 0.f;
static float            gpu_temper = 0.f;

static bool             threadlived = true;
static pthread_t        pt = 0;

static struct termio    tty_old = {0};
static struct termio    tty_new = {0};

/////////////////////////////////////////////////////////////////////////

bool readf( const char* fn, char* data, size_t datalen )
{
    FILE* fp = fopen( fn, "r" );
    if ( fp != NULL )
    {
        fgets( data, datalen, fp );
        fclose( fp );

        return true;
    }

    return false;
}

// detype 
//   0 : CPU
//   1 : GPU
float read_temperature( unsigned dtype )
{
    char dev_map[256] = {0};
    sprintf( dev_map, DEF_VIRTUAL_TEMP_MAP, dtype );

    char data[32] = {0};

    if ( readf( dev_map, data, 32 ) == true )
    {
        size_t datalen = strlen(data) - 1;

        if ( datalen > 4 )
        {
            char tmp_h[4] = {0};
            char tmp_l[4] = {0};

            size_t tmpsz_h = datalen - 3;
            strncpy( tmp_h, data, tmpsz_h );
            strncpy( tmp_l, &data[tmpsz_h], 3 );

            char conv_s[32] = {0};

            sprintf( conv_s, "%s.%s", tmp_h, tmp_l );

            return atof( conv_s );
        }
    }

    return -99.f;
}

void read_cpu_info()
{
    char dev_map[256] = {0};
    char data[64] = {0};

    // Get CPU present min and max.
    if ( readf( DEF_CPU_PRESENT, data, 64 ) == true )
    {
        // split with "-".
        string splitsrc = data;
        int    splpos   = splitsrc.find( "-" );
        if ( splpos != string::npos )
        {
            string cmin = splitsrc.substr( 0, splpos );
            string cmax = splitsrc.substr( splpos + 1 );
            cpu_cnt_min = atoi( cmin.c_str() );
            cpu_cnt_max = atoi( cmax.c_str() );
            cpu_cnt_max++;
        }
    }

    size_t cpu_cnts = cpu_cnt_max - cpu_cnt_min;

    if ( cpu_cnts > 0 )
    {
        cpu_freq_max.resize( cpu_cnts );
        cpu_freq_min.resize( cpu_cnts );
        cpu_freq_cur.resize( cpu_cnts );
    }

    for( unsigned cnt=cpu_cnt_min; cnt<cpu_cnt_max; cnt++ )
    {
        sprintf( dev_map, DEF_CPU_MAX_CLK_MAP, cnt );
    
        if ( readf( dev_map, data, 64 ) == true )
        {
            cpu_freq_max[cnt-cpu_cnt_min] = (float)atoi(data);
        }
        
        sprintf( dev_map, DEF_CPU_MIN_CLK_MAP, cnt );
    
        if ( readf( dev_map, data, 64 ) == true )
        {
            cpu_freq_min[cnt-cpu_cnt_min] = (float)atoi(data);
        }
    }
}

void update_cpu_freq()
{
    char dev_map[256] = {0};

    for( size_t cnt=cpu_cnt_min; cnt<cpu_cnt_max; cnt++ )
    {
        sprintf( dev_map, DEF_CPU_CUR_CLK_MAP, cnt );
    
        char data[64] = {0};

        if ( readf( dev_map, data, 64 ) == true )
        {
            cpu_freq_cur[cnt-cpu_cnt_min] = (float)atoi(data);
        }
    }
}

void update_temperatures()
{
    cpu_temper = read_temperature( 0 );
    gpu_temper = read_temperature( 1 );
}

void print_bases()
{
    printf( "# CPU presented : %u to %u.\n", 
            cpu_cnt_min, 
            cpu_cnt_max-1 );

    for( size_t cnt=cpu_cnt_min; cnt<cpu_cnt_max; cnt++ )
    {
        printf( "\t .. CPU %2u works at %.1f ~ %.1f MHz\n",
                cnt,
                cpu_freq_min[cnt-cpu_cnt_min] / 1000.f,
                cpu_freq_max[cnt-cpu_cnt_min] / 1000.f );
    }
}

void generate_components()
{
}

void init_tty()
{
    ioctl( 0, TCGETA, &tty_old );
    tty_new = tty_old;

    tty_new.c_lflag &= ~(ECHO | ICANON);
    tty_new.c_cc[VMIN] = 0;
    tty_new.c_cc[VTIME] = 1;

    ioctl( 0, TCSETAF, &tty_new );
}

void deinit_tty()
{
    ioctl( 0, TCSETAF, &tty_old );
}

bool async_key( char* k )
{
    int readsz = read( 0, k, 1 );

    if ( readsz > 0 )
    {
        return true;
    }

    return false;
}

// --
// A mutex for signal handling.
static bool sighandling = false;

void sighandler( int sig )
{
    if ( sighandling == false )
    {
        sighandling = true;

        printf( "\nSIGNAL:%d\n",sig );

        threadlived = false;
        pthread_cancel( pt );
        pthread_join( pt, 0 );
        deinit_tty();
        exit( 0 );
    }
}

void* threadRun( void* p )
{
    printf( "\n" );

    while( threadlived == true )
    {
        update_cpu_freq();
        update_temperatures();

        printf( "\r" );
        for( size_t cnt=cpu_cnt_min; cnt<cpu_cnt_max; cnt++ )
        {
            printf( "[%2u:%1.3f Ghz]", 
                    cnt, cpu_freq_cur[cnt-cpu_cnt_min] / 1000000.f );
        }

        if ( cpu_temper >= 0.f )
        {
            printf( "[ C:%3.1f 'C]", cpu_temper );
        }

        if ( gpu_temper >= 0.f )
        {
            printf( "[ G:%3.1f 'C]", gpu_temper );
        }

        fflush( stdout );

        char ak = 0;
        if ( async_key( &ak ) == true )
        {
            if ( ( ak == 'q' ) || ( ak == 'Q' ) || ( ak == 27 ) )
            {
                fflush( stdout );
                break;
            }
        }

        usleep( 1000000 );
    }

    pthread_exit( 0 );
    return NULL;
}

int main( int argc, char** argv )
{
    signal( SIGINT, sighandler );
    init_tty();

    printf( "%s, Version %s\n(C)Copyright 2019 Raphael Kim\n\n",
            DEF_APP_NAME,
            DEF_APP_VERSION );

    generate_components();
    read_cpu_info();

    if ( cpu_cnt_max == 0 )
    {
        printf( "Error: Cannot read CPU counts.\n" );
        exit( 0 );
    }

    print_bases();

    pthread_create( &pt, NULL, threadRun, NULL );
    pthread_join( pt, 0 );

    deinit_tty();
    fflush( stdout );

    return 0;
}

