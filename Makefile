# Makefile for common ARM cpu monitoring information.
# ----------------------------------------------------------------------
# Written by Raph.K.
#

# Compiler preset.
GCC = $(PREFIX)gcc
GPP = $(PREFIX)g++
AR  = $(PREFIX)ar

# TARGET settings
TARGET_PKG = cpuclkmon
TARGET_DIR = ./bin
TARGET_OBJ = ./obj

# Installation paths
INST_PATH  = /usr/local/bin

# Compiler optiops 
COPTS  = -ffast-math -fexceptions -fopenmp -O3 -s

# CC FLAG
CFLAGS  = -I$(SRC_PATH) -I$(FSRC_PATH) 
CFLAGS += -I$(LIB_PATH) -I$(LOCI_PATH)
CFLAGS += $(DEFS)
CFLAGS += $(COPTS)

# LINK FLAG
#LFLAGS  = -static
LFLAGS += -lpthread
#LFLAGS += -lncurses

# Sources
SRC_PATH = src
SRCS = $(wildcard $(SRC_PATH)/*.cpp)

# Make object targets from SRCS.
OBJS = $(SRCS:$(SRC_PATH)/%.cpp=$(TARGET_OBJ)/%.o)

.PHONY: prepare clean

all: prepare clean continue

continue: $(TARGET_DIR)/$(TARGET_PKG)

prepare:
	@mkdir -p $(TARGET_DIR)
	@mkdir -p $(TARGET_OBJ)

clean:
	@echo "Cleaning built targets ..."
	@rm -rf $(TARGET_DIR)/$(TARGET_PKG).*
	@rm -rf $(TARGET_INC)/*.h
	@rm -rf $(TARGET_OBJ)/*.o

install: $(TARGET_DIR)/$(TARGET_PKG)
	@cp -rf $< $(INST_PATH)

uninstall: $(INST_PATH)/$(TARGET_PKG)
	@rm -rf $(INST_PATH)/$(TARGET_PKG)

$(OBJS): $(TARGET_OBJ)/%.o: $(SRC_PATH)/%.cpp
	@echo "Building $@ ... "
	@$(GPP) $(CFLAGS) -c $< -o $@

$(TARGET_DIR)/$(TARGET_PKG): $(OBJS)
	@echo "Generating $@ ..."
	@$(GPP) $(TARGET_OBJ)/*.o $(CFLAGS) $(LFLAGS) -o $@
	@echo "done."

