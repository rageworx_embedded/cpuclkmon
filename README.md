# cpuclkmon

A simple CPU clock monitoring console application. It is desgined for Raspberry Pi3B+ in first time, but useful for other systems, too.

## Requirements
* Linux, or Embedded Linux
* G++ and user level compilation tools.

## How to build
* Just type to 
    `make`
* Then, if succeeded to build,
	`sudo make install`

## Cross compiling
* Just define preprocess with "PREFIX=", and use Makefile as below:
     - `PREFIX=arm-linux-gnueabihf- make clean`
     - `PREFIX=arm-linux-gnueabihf- make all`
